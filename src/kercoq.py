from ipykernel.kernelbase import Kernel

class Kercoq(Kernel):
    implementation = 'Kercoq'
    implementation_version = '1.0'
    language = 'coq'
    language_info = {
        'name': 'coq',
        'mimetype': 'text/x-coq',
        'file_extension': '.v',
    }
    banner = "Kercoq — the coq kernel"

    def do_execute(self, code, silent, store_history=True, user_expressions=None,
                   allow_stdin=False):
        if not silent:
            stream_content = {'name': 'stderr', 'text': 'Not implemented yet'}
            self.send_response(self.iopub_socket, 'stream', stream_content)

        return {'status': 'abort',
                # The base class increments the execution count
                'execution_count': self.execution_count,
               }

if __name__ == '__main__':
    from ipykernel.kernelapp import IPKernelApp
    IPKernelApp.launch_instance(kernel_class=Kercoq)
