;; This file can be used with the Guix package manager to build kercoq.
;; To use a development version with guix, clone this repository and run the
;; following command inside your local checkout:
;;
;;   guix environment --ad-hoc -l guix.scm jupyter
;;
;; Then you will be able to run jupyter-notebook and create a new notebook with
;; the Kercoq kernel.
;;
;; You can also install it in a profile with `guix package -f guix.scm`.

(use-modules (guix build-system gnu)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (guix licenses))

(define %source-dir (dirname (current-filename)))

(package
  (name "kercoq")
  (version "0")
  (source (local-file %source-dir #:recursive? #t
                      #:select? (git-predicate %source-dir)))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f
     #:phases
     (modify-phases %standard-phases
       (delete 'configure)
       (delete 'build)
       (replace 'install
         (lambda* (#:key outputs #:allow-other-keys)
           (let ((out (assoc-ref outputs "out")))
             (invoke "ls")
             (install-file "src/kernel.json"
                           (string-append out "/share/jupyter/kernels/kercoq"))
             (install-file "src/kercoq.py"
                           (string-append out "/lib/python3.7/site-packages"))
             #t))))))
  (home-page "https://framagit.org/Remi/kercoq")
  (synopsis "Coq kernel for the Jupyter Notebook")
  (description "This package provides a Coq kernel for the Jupyter Notebook.  It
allows users to interact with coqtop through Jupyter")
  (license gpl3+))
